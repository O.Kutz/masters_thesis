# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 14:19:17 2020

@author: Oliver Kutz
"""

import os
import pandas as pd
import numpy as np
import csv

class Read_mummer_output:
    def __init__(self, path_A, path_SNP, save_folder):
        """
        

        Parameters
        ----------
        path_A : TYPE string
            A path to the show-coords output
            
        path_to_SNP : TYPE string
            A path to show-snps output
            
        save_folder : TYPE string
            A path to desired output folder
            
            
        Output
        -------
        A .csv formatted file at the desired location containing 4 tables:
        
            Table 1: Overview
                    Columns: [Sample] [IDtotal] [No. Gaps]  [No. Indels] [No. MM]                 
            Table 2: Gaps:
                    Columns: [Sample] [No. Gaps] [Gap 1 length] [Gap 1 start] [Gap 1 end] [Gap n length] [Gap n start] [Gap n end]
                
            Table 3: Indels
                    Columns: [Sample] [No. Indels] [Indel_1_from] [Length_1] [Pos_1_start (Reference-Query)] [Sequence_1] [Indel_x_from] [Length_x] [Pos_x_start (Reference-Query)] [Seq_x]
                
            Table 4: Mismatches (MM)
                    Columns: [Sample] [No. MM] [Pos 1 Ref] [Pos 1 Query] [MM 1 (R-Q)] [Pos X Ref] [Pos X Query] [MM X [R-Q]]



        """
        
        
        all_pathes_alignment = self.get_the_data(path_A)
        all_pathes_SNP = self.get_the_data(path_SNP)
        analyze_all = self.read_the_data(all_pathes_alignment,all_pathes_SNP)
        self.write_dat_table(save_folder, analyze_all)
    
    
    
    
    
    
    
    def get_the_data(self, path):
        """
        

        Parameters
        ----------
        path : TYPE string
            Path to MUMmer output folder 
                either show-coords output or show-snps output

        Returns
        -------
        everything_checked : TYPE list
            A list containing pathes of every sample calculated by either
            show-coords or show-snps

        """
        
        
        everything = os.listdir(path)
        #check if only .csv is in folder
        everything_checked = [path+i for i in everything if i[-4:] == ".csv"]
        return everything_checked
    
    
    
    
    
    
    def read_the_data(self,path_A, path_S):
        """
        

        Parameters
        ----------
        path_A : TYPE list
            A collection of pathes to all results calculated via show-coords
            
        path_S : TYPE list
            A collection of pathes to all results calculated via show-snps

        Returns
        -------
        complex_data : TYPE list of lists
            Extracted information about sample name, found gaps, found indels,
            found mismatches

        """
        
        
        analyzed_data = []
        for i in range(len(path_A)):
            name, gaps, IDtotal, analyze_Indel, analyze_MM = self.analyze_da_data(path_A[i], path_S[i])
            analyzed_data.append([name, gaps, IDtotal, analyze_Indel, analyze_MM])
        complex_data = self.build_complex(analyzed_data)
        return complex_data
        
        
        
    
        
        
    
    def analyze_da_data(self, path_A, path_S):
        """
        

        Parameters
        ----------
        path_A : TYPE string
            Path to show-coords putput
            
        path_S : TYPE
            Path to show-snps output

        Returns
        -------
        name : TYPE string
            Samples name
            
        gaps : TYPE list
            Detailed information about each gap of the samples alignment
            
        IDtotal : TYPE integer
            Calculated identity between query and reference
            
        analyze_Indel : TYPE list
            Detailed information about each indel of the samples alignment
            
        analyze_MM : TYPE list
            Detailed information about each mismatch of the samples alignment

        """

        data_A = pd.read_csv(path_A, header = None, names =  ["[S1]", "[E1]", "[S2]", "[E2]", "[LEN 1]",
                                                              "[LEN 2]", "[% IDY]", "[LEN R]", "[LEN Q]", "[COV R]",
                                                           "[COV Q]", "[TAG 1]", "[TAG 2]", "[TAG 3]"], 
                                                sep = "\t", index_col = False)
        
        name = path_A[path_A.find("ment/")+5:path_A.find("_cont")]
        gaps, IDtotal = self.analyze_gaps(data_A)
        data_S = pd.read_csv(path_S, header = None, names = ["[P1]", "[SUB 1]", "[SUB 2]",
                                                             "[P2]", "[BUFF]", "[DIST]", 
                                                          "[R]", "[Q]", "[LEN R]",
                                                          "[LEN Q]", "[FRM]", "[TAG 1]", 
                                                          "[TAG 2]", "[TAG 3]"],
                                                sep = "\t", index_col = False)
        analyze_Indel = self.analyze_Indel(data_S, name)
        if analyze_Indel is None:
            analyze_Indel = []
        analyze_MM = self.analyze_MM(data_S, name)
        if analyze_MM is None:
            analyze_MM = []
        
        
        return name, gaps, IDtotal, analyze_Indel, analyze_MM




        
        
        
        
        
    def analyze_gaps(self, data):
        """
        

        Parameters
        ----------
        data : TYPE array
            Whole output of show-coords

        Returns
        -------
        gap : TYPE list
            Detailed information about each gap found in show-coords output
            
        IDtotal : TYPE int
            Calculated identity between query sequences (sample contigs)
            and template. 

        """
        
        
        gap = []
        overlaps = []
        ID = 0
        
        contig_begin = data["[S1]"].values[3:]
        contig_end = data["[E1]"].values[3:]
        IDparts = data["[% IDY]"].values[3:]

        for i in range(0,len(contig_begin)):

            if int(contig_begin[0]) > 1:
                gap.append([contig_begin[i], 1, contig_begin[i]])
                ID = ID - int(contig_begin[i])
            
            #Number of identical basepairs of the current fragment
            ID += (int(contig_end[i]) - int(contig_begin[i]) + 1) * float(IDparts[i]) / 100

            if i < len(contig_begin)-1:
                overlaps.append([int(contig_begin[i+1]) - int(contig_end[i]), contig_end[i], contig_begin[i+1]])

        for i in overlaps:
            if int(i[0]) > 0:
                gap.extend(i)

            elif int(i[0]) < 0:
                ID = ID + int(i[0])
        
        #Removing overlapping basepairs from total amount of identical basepairs
        ID = ID - len(overlaps)
        y = data["[LEN R]"].values[3:][0]
        
        #IDtotal = Number of identical basepairs / Total length of reference
        
        IDtotal = ID / int(y)
        IDtotal = round(IDtotal, 3)
        
        return gap, IDtotal
    
    
    
    
    
    

        
    
    
    def analyze_Indel(self, data, name):
        """
        

        Parameters
        ----------
        data : TYPE array
            Whole output from show-snps
            
        name : TYPE string
            Samples name

        Returns
        -------
        indels : TYPE list
            Detailed information about each found indel

        """


        what_is_ref = data["[SUB 1]"].values[3:]
        what_is_q = data["[SUB 2]"].values[3:]
        where_is_it_ref = data["[P1]"].values[3:]
        where_is_it_q = data["[P2]"].values[3:]

        indels = []
        index_start = 0        
        where_to_find = ""        
        epoch = 0
        seq = []
        secure_break = 10
        pos_data = []
        list_length = len(what_is_ref)
        TICKER = 0
        
        while epoch <= list_length:
            TICKER += 1
            
            for i in range(index_start, list_length):

                if where_to_find == "":
                    #Insertion in REF
                    if "." in what_is_ref[i]:
                        where_to_find = "REF"
                        seq.append(what_is_q[i])
                        pos_data.append("REF")
                        epoch = i
                        
                    #Insertion in Q
                    elif "." in what_is_q[i]:
                        where_to_find = "Q"
                        seq.append(what_is_ref[i])
                        pos_data.append("Q",)
                        epoch = i

                elif where_to_find == "REF":
                    if i+1 < list_length and where_is_it_ref[i] == where_is_it_ref[i+1]:
                        seq.append(what_is_q[i])
                    else:
                        seq.append(what_is_q[i])
                        seq = "".join(seq)
                        pos_data.append(len(seq))
                        pos_data.append([where_is_it_ref[index_start], where_is_it_q[index_start]])
                        pos_data.append(seq)
                        where_to_find = ""
                        index_start = i + 1
                        epoch = i
                        seq = []
                        [indels.append(i) for i in pos_data]
                        pos_data = []
                        break
                elif where_to_find == "Q":
                    if i+1 < list_length and where_is_it_q[i] == where_is_it_q[i+1]:
                        seq.append(what_is_ref[i])
                    else:
                        seq.append(what_is_ref[i])
                        seq = "".join(seq)
                        pos_data.append(len(seq))
                        pos_data.append([where_is_it_ref[index_start], where_is_it_q[index_start]])
                        pos_data.append(seq)
                        
                        where_to_find = ""
                        index_start = i + 1
                        epoch = i
                        seq = []
                        [indels.append(i) for i in pos_data]
                        pos_data = []
                        break
            if TICKER == secure_break:
                break
        return indels




   
              
    def analyze_MM(self, data, name):
        """
        

        Parameters
        ----------
        data : TYPE array
            Whole output from show-snps
            
        name : TYPE string
            Samples name

        Returns
        -------
        all_MM : TYPE list
            Detailed information about each mismatch found

        """
        
        
        what_is_in_ref = data["[SUB 1]"].values[3:]
        what_is_in_q = data["[SUB 2]"].values[3:]
        
        all_MM = []
        for i in range(len(what_is_in_ref)):
            if what_is_in_ref[i] != "." and what_is_in_q[i] != ".":
                all_MM.append(data["[P1]"].values[3:][i])
                all_MM.append(data["[P2]"].values[3:][i])
                all_MM.append([what_is_in_ref[i], what_is_in_q[i]])
        if len(all_MM) > 0:
            return all_MM
        else:
            return None
        
    
    
    
    
    
              
    
    def build_complex(self, data):
        """
        

        Parameters
        ----------
        data : TYPE list with lists
            Detailed information about all samples regarding identity between
            query and reference, each gap, each indel, each mismatch. 

        Returns
        -------
        complex_data : TYPE list with lists
            Ordered results with 4 lists. Each positions of each list is ordered
            according to the following scheme:
            List 1: Overview
                    Columns: [Sample] [IDtotal] [No. Gaps]  [No. Indels] [No. MM]                 
            List 2: Gaps:
                    Columns: [Sample] [No. Gaps] [Gap 1 length] [Gap 1 start] [Gap 1 end] [Gap n length] [Gap n start] [Gap n end]
                
            List 3: Indels
                    Columns: [Sample] [No. Indels] [Indel_1_from] [Length_1] [Pos_1_start (Reference-Query)] [Sequence_1] [Indel_x_from] [Length_x] [Pos_x_start (Reference-Query)] [Seq_x]
                
            List 4: Mismatches (MM)
                    Columns: [Sample] [No. MM] [Pos 1 Ref] [Pos 1 Query] [MM 1 (R-Q)] [Pos X Ref] [Pos X Query] [MM X [R-Q]]

        """
        
        
        data_indel = []
        data_MM = []
        total_data = []
        data_gaps = []
        
        for i in data:
            total_data.append([i[0], i[2], len(i[1])/3, len(i[3])/4, len(i[4])/3])
            data_gaps.append([i[0], len(i[1])/3, i[1]])
            data_indel.append([i[0], len(i[3])/4, i[3]])
            data_MM.append([i[0], len(i[4])/3, i[4]])
        
        complex_data = np.array([total_data, data_gaps, data_indel, data_MM])
        return complex_data






     
        
    def create_da_header(self, data):
        """
        

        Parameters
        ----------
        data : TYPE list with lists
            Complete output from all functions

        Returns
        -------
        header_total : TYPE list
            Header for table 1: Overview
    
        header_gaps : TYPE list
            Header for table 2: Gaps
            
        header_indels : TYPE list
            Header for table 3: Indels
            
        header_MM : TYPE list
            Header for table 4: Mismatches

        """
        
        
        header_total = ["Sample", "IDtotal", "No. Gaps",  "No. Indels", "No. MM"]
        header_gaps = ["Sample", "No. Gaps"]
        header_indels = ["Sample", "No. Indels"]
        header_MM = ["Sample", "No. MM"]

        longest_gap = int(max([len(i[2])/3 for i in data[1]]))
        most_indels = int(max([len(i[2])/4 for i in data[2]]))
        most_MM = int(max([len(i[2])/3 for i in data[3]]))
        
        [header_gaps.extend(["Gap "+str(i)+" L", "Gap "+str(i) +" Start", "Gap "+str(i)+" End"]) for i in range(1, longest_gap +1)]
        [header_indels.extend(["Indel "+str(i)+" From", "Length "+str(i), "Pos_start "+str(i)+" (Ref-Q)", "Seq"+str(i)]) for i in range(1, most_indels +1)]
        [header_MM.extend(["Pos "+str(i)+" Ref", "Pos "+str(i) +" Q", "MM "+str(i)+" (Ref-Q)"]) for i in range(1, most_MM +1)]
        
        return header_total, header_gaps, header_indels, header_MM





    
    
    def write_dat_table(self, path, data):
        """
        

        Parameters
        ----------
        path : TYPE string
            A path to desired output folder
            
        data : TYPE list with lists
            Complete output from all functions

        Output
        -------
            A .csv formatted file in the desired location containing 4 tables:
            
            Table 1: Overview
                    Columns: [Sample] [IDtotal] [No. Gaps]  [No. Indels] [No. MM]                 
            Table 2: Gaps:
                    Columns: [Sample] [No. Gaps] [Gap 1 length] [Gap 1 start] [Gap 1 end] [Gap n length] [Gap n start] [Gap n end]
                
            Table 3: Indels
                    Columns: [Sample] [No. Indels] [Indel_1_from] [Length_1] [Pos_1_start (Reference-Query)] [Sequence_1] [Indel_x_from] [Length_x] [Pos_x_start (Reference-Query)] [Seq_x]
                
            Table 4: Mismatches (MM)
                    Columns: [Sample] [No. MM] [Pos 1 Ref] [Pos 1 Query] [MM 1 (R-Q)] [Pos X Ref] [Pos X Query] [MM X [R-Q]]


        """
        
        
        header_total, header_gaps, header_indels, header_MM = self.create_da_header(data)
        with open(path, "w", newline = "") as csv_file:
            writer = csv.writer(csv_file, delimiter = ",")

            writer.writerow(header_total)
            for total in data[0]:
                writer.writerow(total)
                      
            writer.writerow("")
            writer.writerow("")
            writer.writerow(header_gaps)
            for gaps in data[1]:
                list_gaps = []
                if gaps[2] is not None:
                    list_gaps = [gaps[0], gaps[1]]
                    list_gaps.extend(gaps[2])
                    writer.writerow(list_gaps)
            
            writer.writerow("")
            writer.writerow("")
            writer.writerow(header_indels)
            for indel in data[2]:
                if indel[2] is not None:
                    list_indel = [indel[0], indel[1]]
                    list_indel.extend(indel[2])
                    writer.writerow(list_indel)
            
            writer.writerow("")
            writer.writerow("")
            writer.writerow(header_MM)
            for MM in data[3]:
                if MM[2] is not None:
                    list_MM = [MM[0], MM[1]]
                    list_MM.extend(MM[2])
                    writer.writerow(list_MM)
        
        
        




if __name__ == "__main__":
    #IMPORTANT: The structure of MUMmer outputs MUST be equal to the structure
    #shown in the masters thesis
    path_to_folder = input("Enter the path to the MUMmer complete output")
    
    subfolder = os.listdir(path_to_folder)
    
    for runs in subfolder:
        path_A = path_to_folder + runs + "/03_Results/03_Stats/01_Alignment/"
        path_SNP = path_to_folder + runs + "/03_Results/03_Stats/02_SNP/"
        path_to_save = path_to_folder + runs + "/" + runs[runs.find("_")+1:] + "_analysis.csv"
        Read_mummer_output(path_A, path_SNP, path_to_save)
        
    
    
    
    
    

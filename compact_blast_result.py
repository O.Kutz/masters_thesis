# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 11:01:53 2019

@author: Oliver Kutz
"""

import pandas as pd
import csv

class compact_blast_result:

    def __init__(self,path_b, path_w):
        """
        

        Parameters
        ----------
        path_b : TYPE string
            A path to the BLAST output
            
        path_w : TYPE string
            A path to desired output folder
            
            
        Output
        -------
        self.finished_blast : TYPE list in lists
            Each BLAST hit is evalued and filtered concerning RFI. The output
            is csv formatted.

        """
        self.categories_blast = ["Sample","Node", "Plasmid", "Qlen", "Length",
                                 "E-value", "pIDent", "nIDent", "RltQlenID"]

        self.rewritten_results = self.identify_blast_result(path_b)
        
        self.finished_blast = self.look_at_alignment(self.rewritten_results)
        
        self.write_da_table(path_w, path_b)
        
    def identify_blast_result(self, path_b):
        """
        

        Parameters
        ----------
        path : TYPE string
            A path to input file (BLAST result)

        Returns
        -------
        rewritten_results : TYPE list in lists
            The description of the contigs was divided into name (NODE number) 
            and contig length.
            Each row in the input data is now a list.

        """
        data = pd.read_csv(path_b, sep = "\t", header = None).values
        rewritten_results = []
        
        for values in data:
            new_data = []
            new_data.append(values[0][:values[0].find("-")])
            new_data.append(values[0][values[0].find("-"):])
            if "length" in new_data[1]:
                new_data[1] = new_data[1][new_data[1].find("NODE_")+5:new_data[1].find("_length")]
            else:
                new_data[1] = new_data[1][new_data[1].find("_")+1:]
            
            for more_data in values[1:]:
                new_data.append(more_data)
                
            rewritten_results.append(new_data)
        return rewritten_results
    

    
    def look_at_alignment(self):
        """
        

        Parameters
        ----------
        NONE.
        

        Returns
        -------
        new_data : TYPE list in lists
            A reduced BLAST output. The RFI is calculated for each BLAST hit. 
            All BLAST hits with a RFI below 0.95 are sorted out.  If a contig 
            shows several BLAST hits that achieve a RFI of more than 0.95, 
            only the hit with the highest RFI is output. If several BLAST hits
            show a equally good RFI, they are all output

        """
        used_data = self.rewritten_results[:]
        finished_data = []        
        ticker = 0
        startpoint = 0
        length_table = len(used_data)
        
        for i in range(length_table-1):
            if startpoint == 0:
                RFI = int(used_data[i][7]) / int(used_data[i][3])
                if RFI >= 0.95:
                    finished_data.append(used_data[i] + [RFI])
                    startpoint += 1
            elif finished_data[ticker][0] == used_data[i][0]:
                RFI_1 = int(finished_data[ticker][7]) / int(finished_data[ticker][3])
                RFI_2 = int(used_data[i][7]) / int(used_data[i][3])

                if RFI_2 > RFI_1 and RFI_2 >= 0.95:
                    finished_data[ticker] = used_data[i] + [RFI_2]
                elif RFI_2 == RFI_1 and RFI_2 >= 0.95:
                    finished_data.append(used_data[i] + [RFI_2])
                    ticker += 1
            elif finished_data[ticker][0] != used_data[i][0]:
                RFI = int(used_data[i][7]) / int(used_data[i][3])
                if RFI >= 0.95:
                    finished_data.append(used_data[i] + [RFI])
                    ticker += 1

        return finished_data
    
    
    def write_da_table(self, path_w, path_b):
        """
        

        Parameters
        ----------
        path_w : TYPE string
            A path to the desired output folder
            
        path_b : TYPE string
            A path to input BLAST file

        Output
        -------
        A .csv file containing filtered BLAST results together with
        calculated RFI.
        

        """
        sample_name = path_b
        while "/" in sample_name:
            sample_name = sample_name[sample_name.find("/")]
        output_file = path_w+sample_name+"_auswertung.csv"
        with open(output_file,"w",newline="") as csv_file:
            writer = csv.writer(csv_file, delimiter = ",")
            writer.writerow(self.categories_blast)
            for i in self.finished_blast:
                writer.writerow(i)

path_b = input("Enter the path to the BLAST result.")
path_w = input("Enter the path to the desired output folder.")
   

compact_blast_result(path_b, path_w)